$(document).ready(function($) {
	$("#executar").click(function(event) {
		
		//Append
		$("ul").append('<li>Dentro e na última posição</li>');

		//Prepend
		$("ul").prepend('<li>Dentro e na primeira posição</li>');

		//Before
		$("ul").before('<h1>Fora e antes do elemento alvo</h1>');

		//After
		$("ul").after('<h1>Fora e depois do elemento alvo</h1>');

		//Wrap (Envolve o elemento alvo)
		$("ul").wrap('<p></p>');

		//Clone
		$("ul").append($("li").clone());							
	});
});