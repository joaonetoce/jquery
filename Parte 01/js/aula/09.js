$(document).ready(function($) {
	$("#executar").click(function(event) {
		//Recupera o valor de um atributo
		//alert($("img").attr('title'));

		//Alterar o valor de um atributo
		//$("img").attr('src', "img/10.jpeg")
		//.attr('title', "Norcon");

		//Outra forma(Formato JSON)	
		//$("img").attr({
		//	src: 'img/10.jpeg',
		//	title: 'Norcon'
		//});

		//Função que remove um atributo
		//$("img").removeAttr('src');

		//Adicionar classe
		//$("img").addClass('destaque');

		//Verifica se a classe existe (retorna um bool)
		//alert($("img").hasClass('destaque'));

		//Alterar (toogleClass)
		$("img").toggleClass('destaque');		
	});
});