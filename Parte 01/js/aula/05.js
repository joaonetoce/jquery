$(document).ready(function($) {
	//$("*").css('background', '#CCC');
	//$("tbody tr").css('background', '#CCC');
	
	//Marca apenas a primeira tr
	//$("tbody tr:first").css('background', '#CCC');
	
	//Marca apenas a útima tr
	//$("tbody tr:last").css('background', '#CCC');
	
	//Marcar todas as tr menos última utilizando o not
	//$("tbody tr:not(tbody tr:last)").css('background', '#CCC');

	//Buscar a ocorrências pares
	//$("tbody tr:even").css('background', '#CCC');

	//Buscar a ocorrências ímpares
	//$("tbody tr:odd").css('background', '#999');	

	//Busca ocorrência específica da coleção
	//$("tbody tr:eq(3)").css('background', '#CCC');

	//Busca ocorrência maior do que um número specificado
	//$("tbody tr:gt(2)").css('background', '#CCC');

	//Busca ocorrência menor do que um número specificado
	//$("tbody tr:lt(3)").css('background', '#CCC');
});