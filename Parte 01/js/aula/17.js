$(document).ready(function($) {
	//$(":text").keyup(function(event) {		
	//	$("#msg").text($(this).val());
	//});

	//Equivalente a função Keyup
	//$(":text").bind("keyup", function(event) {		
	//	$("#msg").text($(this).val());
	//});

	//Outra forma de escrever a função
	function msg(event){
		$("#msg").text($(this).val());
	} 

	//Desse formato bastaria chamar as funçõe utilizadas através do parametro
	//$(":text").keyup(msg).focus(msg).blur(msg);
	$(":text").bind("keyup focus blur", msg);

	//Desvincula elemento de uma função (unbind)
	$(":button").bind("click", function(){
		$(":text").unbind('keyup', msg);
		$(":text").val('Texto inclusivo via JQuery');
		$(":text").trigger('focus');
	});
});