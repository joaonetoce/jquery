$(document).ready(function($) {
	$("#btnHide").click(function(event) {
		//$("div").hide(); //Sem efeito
		//$("div").hide("slow"); //devagar
		//$("div").hide("fast"); //Rápido
		$("div").hide(2000); //em milisegundos
	});

	$("#btnShow").click(function(event) {
		//$("div").show();
		$("div").show("slow");
		//$("div").show("fast");
		//$("div").show(2000);
	});

	$("#btnToggle").click(function(event) {
		//$("div").toggle();
		$("div").toggle("slow");
		//$("div").toggle("fast");
		//$("div").toggle(2000);
	});
});