$(document).ready(function($) {
	//Marcar os textos que contém determinado conteúdo
	//$("td:contains(Comédia)").css('background', '#CCC');

	//Marcar as células vazias
	//$("td:empty").css('background', '#CCC');

	//Marcar as células que não estejam vazias
	//$("td:not(td:empty)").css('background', '#CCC');	

	//Buscar um determonado elemento dentro de um elemento
	$("td:has(strong)").css('background', '#CCC');	
});