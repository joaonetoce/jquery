$(document).ready(function($) {
	//Buscar elementos pelo seu atributo
	//$("a[name]").css('background', '#CCC');

	//Buscar elementos pelo VALOR do seu atributo
	//$("a[name=menu]").css('background', '#CCC');

	//Buscar elementos pelo texto contido(*) no VALOR do seu atributo
	//$("a[href*=min]").css('background', '#CCC');

	//Buscar elementos pelo texto contido no VALOR do seu atributo que termine($) com determinado texto
	//$("a[href$=br]").css('background', '#CCC');

	//Buscar elementos pelo texto contido no VALOR do seu atributo que comece(^) com determinado texto
	//$("a[href^=mailto]").css('background', '#CCC');

	//Buscar elementos quando o VALOR do seu atributo é diferente pelo valor definido
	$("a[name!=menu]").css('background', '#CCC');
});