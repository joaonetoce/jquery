$(document).ready(function($) {
	//Seletor de descendentes
	$("div span").css('background-color', 'yellow');
	//Seleciona a filha(>)  "ul" que está dentro de "li"
	$("li > ul").css('color', 'red');
	//Irmão do elemento: sinal de (+) para pegar o próximo elemento irmão e sinald e (~) para pegar todos os elementos irmãos
	$("#item1_1 ~ li").css('color', 'blue');
});