$(document).ready(function($) {
	//$("#filmes tbody tr").addClass('impar');
	//$("#filmes tbody tr:odd").addClass('impar'); //pintar apenas as linhas impares
	//$("#filmes tbody tr:nth-child(3)").addClass('impar'); //indica a posição do indice
	//$("#filmes tbody tr:nth-child(odd)").addClass('impar'); 
	//$("#filmes tbody tr:nth-child(2n+1)").addClass('impar');

	//Zebrar de 2 em duas linhas
	//$("#filmes tbody tr:nth-child(4n-1)").addClass('impar');
	//$("#filmes tbody tr:nth-child(4n)").addClass('impar');

	 /*
		$("#filmes tbody tr").hover(function() {
	 	$(this).addClass('destaque')
	 }, function() {
	 	$(this).removeClass('destaque');
	 });
	 */
	 $("#filmes thead tr th").not('th:first-child')
	 $("#filmes thead tr th").prepend('<span>+</span>').css('cursor', 'pointer');
	 $("#filmes thead tr th").each(function(i) {
	 	var n = i;
	 	$(this).click(function() {

	 		$("td").removeClass('destaque');

	 		$(this).parents("thead").siblings('tbody').children('tr').each(function() {
	 			$(this).children('td:eq(' + n + ')').addClass('destaque');
	 		});
	 		//alert("Click: " + n);
	 	});
	 });
});